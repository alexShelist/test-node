const server = require('../../server/main');
const logger = require('../lib/logger');


logger.info('Starting server...');
server.listen(8000, () => {
  logger.success('Server is running at http://localhost:8000')
})
