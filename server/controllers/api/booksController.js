const db = require('../../models/index');


function getBooks(req, res) {
    return db.books.findAll({
        include: [
            db.authors,
        ]
    }).then((books) => {
        res.json(books);
    }).catch((e) => {
        res.json({ error: e });
    });
}

function getBookById(req, res) {
    return db.books.findOne({
        where: {
            id: req.params.id
        },
        include: [
            db.authors,
        ]
    }).then((books) => {
        res.json(books);
    }).catch((e) => {
        res.json({ error: e });
    });
}

function updateBook(req, res) {
    const { title, authorId, id } = req.body;
    return db.books.update({
        title,
        authorId,
    }, {
        where: {
            id
        },
        returning: true}).then(() => {
        res.json({ success: true });
    }).catch((e) => {
        res.json({ error: e });
    });
}

function deleteBook(req, res) {
    const { id } = req.params;
    return db.books.destroy({
        where: {
            id
        },
    }).then(() => {
        res.json({ success: true });
    }).catch((e) => {
        res.json({ error: e });
    });
}

module.exports = { getBooks, getBookById, updateBook, deleteBook };