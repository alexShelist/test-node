const db = require('../models/index');
const path = require('path');


function getAllBooks(req, res) {
    return db.books.findAll({
        include: [
            db.authors,
        ]
    }).then((books) => {
            res.render('pages\\index', {
                page: 'Main page',
                all: books,
                isAdmin: req.session.isAdmin,
            });
    }).catch((e) => {
        console.log("Error:", e);
    });
}

function upadteBookById(req, res) {
    const { id } = req.params;
    if (!req.session.isAdmin) {
        res.redirect('/');
    }
    return db.books.findOne({
        where: {
            id: id
        },
        include: [
            db.authors,
        ]
    }).then((book) => {
        return db.authors.findAll({
        }).then((authors) => {
            res.render('pages\\books\\update', {
                page: 'Update Book',
                book: book || {},
                authors,
                isAdmin: req.session.isAdmin,
            });
        }).catch((e) => {
            console.log("Error:", e);
        });
    }).catch((e) => {
        res.json({ error: e });
    });
}

function updateBook(req, res) {
    const { title, authorId, id } = req.body;
    if (!req.session.isAdmin) {
        res.redirect('/');
    }
    if (id) {
        return db.books.update({
            title,
            authorId,
        }, {
            where: {
                id
            },
            returning: true
        }).then(() => {
            res.redirect('/');
        }).catch((e) => {
            console.log("Error:", e);
        });
    }
    return db.books.create({
        title,
        authorId,
    }).then(() => {
        res.redirect('/');
    }).catch((e) => {
        console.log("Error:", e);
    });

}

function deleteBook(req, res) {
    const { id } = req.params;
    if (!req.session.isAdmin) {
        res.redirect('/');
    }
    return db.books.destroy({
        where: {
            id
        },
    }).then(() => {
        res.redirect('/');
    }).catch((e) => {
        res.json({ error: e });
    });
}

module.exports = { getAllBooks, upadteBookById, updateBook, deleteBook };