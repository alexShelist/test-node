const path = require('path');
const db = require('../models/index');


function getAllAuthors(req, res) {
    return db.authors.findAll({
        include: [
            db.books,
        ]
    }).then((authors) => {
            res.render('pages\\authors\\index', {
                page: 'Authors',
                all: authors,
                isAdmin: req.session.isAdmin,
            });
    }).catch((e) => {
        console.log("Error:", e);
    });
}

function getAuthorById(req, res) {
    if (!req.session.isAdmin) {
        res.redirect('/authors');
    }
    return db.authors.findOne({
        where: {
            id: req.params.id
        }
    }).then((author) => {
        res.render('pages\\authors\\update', {
            page: 'Update author',
            author: author || {},
            isAdmin: req.session.isAdmin,
        });
    }).catch((e) => {
        console.log("Error:", e);
    });
}

function updateAuthor(req, res) {
    const { name, id } = req.body;
    if (!req.session.isAdmin) {
        res.redirect('/authors');
    }
    if (id) {
        return db.authors.update({
            name,
        }, {
            where: {
                id
            },
            returning: true
        }).then(() => {
            res.redirect('/authors');
        }).catch((e) => {
            res.json({error: e});
        });
    }
    return db.authors.create({
        name,
    }).then(() => {
        res.redirect('/authors');
    }).catch((e) => {
        console.log("Error:", e);
    });
}

function deleteAuthor(req, res) {
    const { id } = req.params;
    if (!req.session.isAdmin) {
        res.redirect('/authors');
    }
    if (!req.session.isAdmin) {
        res.render('pages\\authors\\index', {
            page: 'Authors',
            all: authors,
            isAdmin: req.session.isAdmin,
        });
    }
    return db.authors.destroy({
        where: {
            id
        },
    }).then(() => {
        res.redirect('/authors');
    }).catch((e) => {
        console.log("Error:", e);
    });
}

module.exports = { getAllAuthors, getAuthorById, updateAuthor, deleteAuthor };