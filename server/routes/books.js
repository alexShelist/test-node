const express = require('express');
const router = express.Router();
const controller = require('../controllers/booksController');


router.get('/', controller.getAllBooks);
router.get('/books/update/:id', controller.upadteBookById);
router.get('/book/add', controller.upadteBookById);
router.post('/books/save', controller.updateBook);
router.get('/books/delete/:id', controller.deleteBook);


module.exports = router;