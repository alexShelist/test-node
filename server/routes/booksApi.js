const express = require('express');
const router = express.Router();
const controller = require('../controllers/api/booksController');
//const authenticate = require('../middlewares/authenticate');


router.get('/list', controller.getBooks);
router.get('/:id', controller.getBookById);

/**
 * использую для update метод POST т.к. это в задании, семантически верным считаю использовать PATCH
 */
router.post('/update', controller.updateBook);
router.delete('/:id', controller.deleteBook );


module.exports = router;