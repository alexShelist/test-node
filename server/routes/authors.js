const express = require('express');
const router = express.Router();
const controller = require('../controllers/authorsController');


router.get('/', controller.getAllAuthors);
router.get('/update/:id', controller.getAuthorById);
router.post('/save', controller.updateAuthor);
router.get('/add', controller.getAuthorById);
router.get('/delete/:id', controller.deleteAuthor);


module.exports = router;