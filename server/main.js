const express = require('express');
const db = require('./models');
const bodyParser = require('body-parser');

const booksApi = require('./routes/booksApi');
const books = require('./routes/books');
const authors = require('./routes/authors');
const session = require('express-session')

const app = express();

// app.use(compress());
db.sequelize
    .sync()
    .then(() => {
        console.log('Соединение установлено.');
    })
    .catch(err => {
        console.error('Ошибка соединения:', err);
    });
app.engine('ejs', require('ejs').renderFile);
app.set('view engine', 'ejs');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(session({ isAdmin: false,  secret: 'keyboard cat'}))
app.use('/', books);
app.use('/authors', authors);
app.use('/api/v1/books', booksApi);

app.use('/easy/login', (req, res, next) => {
    req.session.isAdmin = true;
    res.redirect('/');
});

app.use('/easy/logout', (req, res, next) => {
    req.session.isAdmin = false;
    res.redirect('/');
});


app.use('*', function (req, res, next) {
    res.redirect('/')
});


module.exports = app;
