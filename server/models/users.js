module.exports = function (sequelize, DataTypes) {
    return sequelize.define('users', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        email: {
            type: DataTypes.STRING,
            unique: true
        },
        password: {
            type: DataTypes.STRING,
            select: false
        },
        name: DataTypes.STRING,
        surname: DataTypes.STRING,
        description: DataTypes.TEXT,
    });
};